/*
 * #%L
 * mosaic-examples-simple-cloudlets
 * %%
 * Copyright (C) 2010 - 2012 Institute e-Austria Timisoara (Romania)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package eu.mosaic_cloud.examples.cloudlets.simple;


import java.io.Serializable;


public final class AuthenticationToken
		implements
			Serializable
{
	public AuthenticationToken (final String token)
	{
		super ();
		this.token = token;
	}
	
	public String getToken ()
	{
		return this.token;
	}
	
	@Override
	public String toString ()
	{
		return this.token;
	}
	
	private final String token;
	private static final long serialVersionUID = 8212390577294189529L;
}
